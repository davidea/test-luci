module("luci.controller.test.file_controller_test", package.seeall)  --notice that new_tab is the name of the file new_tab.lua
 function index()
     entry({"admin", "nome_logico_menu"}, firstchild(), "Nome_visualizzato_menu", 60).dependent=false  --this adds the top level tab and defaults to the first sub-tab (tab_from_cbi), also it is set to position 30
     entry({"admin", "nome_logico_menu", "sottomenu_cbi"}, cbi("cbi-mymodule/cbi_file"), "CBI sottomenu",1)  --this adds the first sub-tab that is located in <luci-path>/luci-myapplication/model/cbi/myapp-mymodule and the file is called cbi_tab.lua, also set to first position
     entry({"admin", "nome_logico_menu", "sottomenu_view"}, template("view-mymodule/view_file"), "View sottomenu",2)  --this adds the second sub-tab that is located in <luci-path>/luci-myapplication/view/myapp-mymodule and the file is called view_tab.htm, also set to the second position
end
local function action_test()
    luci.http.write("Haha, rebooting now...stop")
end
